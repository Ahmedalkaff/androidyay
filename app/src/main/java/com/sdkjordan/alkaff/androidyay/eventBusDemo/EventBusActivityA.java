package com.sdkjordan.alkaff.androidyay.eventBusDemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sdkjordan.alkaff.androidyay.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;


public class EventBusActivityA extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    TextView  mTextView ;
    Button mButton ;
    ToggleButton mToggleButton ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_bus_activity);

        mTextView = findViewById(R.id.textView2);
        mButton = findViewById(R.id.button) ;

        mButton.setOnClickListener(this);


        mToggleButton = findViewById(R.id.toggleButton);
        mToggleButton.setOnCheckedChangeListener(this);

     //  EventBus.getDefault().register(this);
    }

    @Override
    protected void onDestroy() {
        if(EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Subscribe
    public void OnMessageEvent(MessageEvent messageEvent)
    {
        if(messageEvent != null )
            mTextView.setText(messageEvent.getMsg());

    }
    @Subscribe
    public void onOtherEvent(OtherEvent otherEvent)
    {
        mTextView.setText("Other event");
    }
    @Override
    public void onClick(View v) {
        startActivity(new Intent(EventBusActivityA.this,EventBusActivityB.class));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked) {
            if (!EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().register(this);
        }
        else {
            if(EventBus.getDefault().isRegistered(this))
                EventBus.getDefault().unregister(this);
        }
    }
}
