package com.sdkjordan.alkaff.androidyay;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;

import com.sdkjordan.alkaff.androidyay.helpers.MyDataBaseHelper;
import com.sdkjordan.alkaff.androidyay.helpers.ValidationRegs;

public class UserActivity extends AppCompatActivity {


    private EditText mEditTextName, mEditTextPassword, mEditTextPhone, mEditTextEmail;

    private ListView mListView;
    private SimpleCursorAdapter mAdapter;

    MyDataBaseHelper mDataBaseHelper;
    SQLiteDatabase mWritableDatabase;
    private static final String[] from = {MyDataBaseHelper.USER_NAME, MyDataBaseHelper.USER_PHONE, MyDataBaseHelper.USER_EMAIL};
    private static final int[] to = {R.id.textViewName, R.id.textViewPhone, R.id.textViewEmail};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        init();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = mEditTextName.getText().toString();
                String password = mEditTextPassword.getText().toString();
                String email = mEditTextEmail.getText().toString();
                String phone = mEditTextPhone.getText().toString();
                if (validateData(name, password, email, phone)) {
                    boolean isInserted = insertDataToDataBase1(name, password, email, phone);
                    if (isInserted) {
                        showSnakBar("User is inserted", Snackbar.LENGTH_LONG, view);
                        mAdapter.changeCursor(mDataBaseHelper.getAllUsers());
                    } else {
                        showSnakBar("Error inserting user info", Snackbar.LENGTH_LONG, view);
                    }
                } else {
                    showSnakBar("Invalid data input", Snackbar.LENGTH_INDEFINITE, view);
                }
            }
        });
    }

    private void init() {

        mEditTextName = findViewById(R.id.editTextName);
        mEditTextPassword = findViewById(R.id.editTextPassword);
        mEditTextPhone = findViewById(R.id.editTextPhone);
        mEditTextEmail = findViewById(R.id.editTextEmail);

        mDataBaseHelper = new MyDataBaseHelper(getApplicationContext());
        mWritableDatabase = mDataBaseHelper.getWritableDatabase();

        mListView = findViewById(R.id.listview);

        Cursor c = mDataBaseHelper.getAllUsers();
        mAdapter = new SimpleCursorAdapter(getApplicationContext(), R.layout.user_info_layout, c, from, to, SimpleCursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

    }

    private boolean insertDataToDataBase(String name, String password, String email, String phone) {
        if (mDataBaseHelper == null)
            mDataBaseHelper = new MyDataBaseHelper(getApplicationContext());

        if (mWritableDatabase == null || !mWritableDatabase.isOpen() || mWritableDatabase.isReadOnly())
            mWritableDatabase = mDataBaseHelper.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MyDataBaseHelper.USER_NAME, name);
        values.put(MyDataBaseHelper.USER_PASSWORD, password);
        values.put(MyDataBaseHelper.USER_EMAIL, email);
        values.put(MyDataBaseHelper.USER_PHONE, phone);
        long rowID = mWritableDatabase.insert(MyDataBaseHelper.TABLE_NAME, null, values);

        return rowID != -1;
    }

    private boolean insertDataToDataBase1(String name, String password, String email, String phone) {
        if (mDataBaseHelper == null)
            mDataBaseHelper = new MyDataBaseHelper(getApplicationContext());

        long rowID = mDataBaseHelper.AddUser(name, password, email, phone);

        return rowID != -1;
    }

    private void showSnakBar(String invalid_data_input, int Duration, View view) {
        final Snackbar snackbar = Snackbar.make(view, "Invalid data input", Duration);
        snackbar.setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    private boolean validateData(String name, String password, String email, String phone) {

        if (name.length() <= 3)
            return false;

        if (!email.matches(ValidationRegs.EMAIL))
            return false;

        if (!password.matches(ValidationRegs.PASSWORD))
            return false;

        if (!phone.matches(ValidationRegs.PHONE))
            return false;

        return true;

    }


}
