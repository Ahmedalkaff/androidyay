package com.sdkjordan.alkaff.androidyay;

import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;

import com.sdkjordan.alkaff.androidyay.helpers.Constants;
import com.sdkjordan.alkaff.androidyay.listeners.MyWebSocketListener;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.WebSocket;

public class ReciverActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    ToggleButton mToggleButtonRegister;
    Button mButtonSend;

    IntentFilter intentFilter = new IntentFilter(Constants.Receiver.ACTION);
    MyReceiver myReceiver = new MyReceiver();

    boolean isRegistered = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reciver);

        init();

    }

    private void init() {
        mToggleButtonRegister = findViewById(R.id.toggleButtonRegister);
        mButtonSend = findViewById(R.id.buttonSend);

        mToggleButtonRegister.setOnCheckedChangeListener(this);
        mButtonSend.setOnClickListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        Object tag = mToggleButtonRegister.getTag();
        boolean flag = tag == null ? false : Boolean.valueOf(tag.toString());
        if (flag)
            return;

        if (isChecked && !isRegistered) {
            registerReceiver(myReceiver, intentFilter);
            Log.d(Constants.TAG, "myReceiver is registered");
            isRegistered = true;
        } else if (isRegistered) {
            unregisterReceiver(myReceiver);
            Log.d(Constants.TAG, "myReceiver is Unregistered");
            isRegistered = false;
        }
    }

    @Override
    protected void onRestart() {
        mToggleButtonRegister.setChecked(isRegistered);
        mToggleButtonRegister.setTag(true);
        super.onRestart();
    }

    @Override
    protected void onStop() {
        if (myReceiver != null && isRegistered)
            unregisterReceiver(myReceiver);
        super.onStop();
    }

    @Override
    public void onClick(View v) {
//        Intent  intent1 = new Intent(ReciverActivity.this,MyReceiver.class);
//        intent1.putExtra(Constants.Receiver.DATA_KEY,"Intent 1");
//        sendBroadcast(intent1);
        Intent intent2 = new Intent(Constants.Receiver.ACTION);
        intent2.putExtra(Constants.Receiver.DATA_KEY, "Intent 2");
        sendBroadcast(intent2);
    }


    private void start() {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url("ws://echo.websocket.org").build();
        MyWebSocketListener listener = new MyWebSocketListener();
        WebSocket ws = client.newWebSocket(request, listener);

        ws.close(1000,"bye");


        client.dispatcher().executorService().shutdown();
    }
}
