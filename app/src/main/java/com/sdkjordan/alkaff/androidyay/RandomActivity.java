package com.sdkjordan.alkaff.androidyay;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Random;

public class RandomActivity extends AppCompatActivity {

    private static final String HIGHEST_SCORE_KEY = "highest";
    private Random random;
    int highest = 0;
    TextView mtextViewRandom, mtextViewHighest;
    EditText  editText ;

    SharedPreferences mSharedPreferences;
    // ConstraintLayout constraintLayout;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //constraintLayout = findViewById(R.id.parent);
        random = new Random();

        //mSharedPreferences = getSharedPreferences("RandomActivity",MODE_PRIVATE);
        mSharedPreferences = getPreferences(MODE_PRIVATE);          // RandomActivity.xml

        mtextViewRandom = findViewById(R.id.textViewRandom);
        mtextViewHighest = findViewById(R.id.textViewHighest);
        editText = findViewById(R.id.editText3);

        // load highest value
        highest = mSharedPreferences.getInt(HIGHEST_SCORE_KEY, 0);
        mtextViewHighest.setText(String.valueOf(highest));

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int rand = random.nextInt(1000);
                mtextViewRandom.setText(String.valueOf(rand));

                if (rand > highest) {
                    highest = rand;
                    mtextViewHighest.setText(String.valueOf(highest));
                    saveHighestScore(highest);
                }

            }
        });


       // writeToFile("mFile.txt","this is the text");

        readFromFile("scores.txt");
    }

    private void readFromFile(String file) {
        BufferedReader reader = null ;
        try {
            reader = new BufferedReader(new InputStreamReader(openFileInput(file)));
            String line = "";

            while((line = reader.readLine()) != null)
            {
                editText.append(line+"\n");
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void writeToFile(String filename, String text)
    {
        PrintWriter writer = null;
        int mode = 0;
        try {
            File file = new File(getFilesDir() + "/" + filename);
            if (file.exists()) {
                mode = MODE_APPEND;
            }

            writer = new PrintWriter(openFileOutput(filename, mode));
            writer.println(text);

            writer.flush();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (writer != null)
                writer.close();
        }
    }

    private void saveHighestScore(int highest) {

        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putInt(HIGHEST_SCORE_KEY, highest);

        String msg = "";
        if (editor.commit())
            msg = "Highest number is stored:" + highest;
        else
            msg = "Error storing highest number";


        Snackbar.make(fab, msg, Snackbar.LENGTH_LONG)
                .setAction("OK", null).show();

        writeToFile("scores.txt",String.valueOf(highest));
    }

}
