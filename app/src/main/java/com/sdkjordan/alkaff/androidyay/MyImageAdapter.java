package com.sdkjordan.alkaff.androidyay;

import android.content.Context;
import android.provider.ContactsContract;
import android.support.constraint.ConstraintLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.sdkjordan.alkaff.androidyay.helpers.Record;

import java.util.ArrayList;

public class MyImageAdapter extends BaseAdapter {

    private ArrayList<Record> data = new ArrayList<>();

    private Context mContext ;

    public MyImageAdapter(Context mContext) {
        this.mContext = mContext;
    }


    public void AddRecord(Record record)
    {
        data.add(record);

    }
    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Record getItem(int position) throws ArrayIndexOutOfBoundsException {
        if (position >= 0 && position < data.size())
            return data.get(position);
        throw new ArrayIndexOutOfBoundsException("Invalid index value :" + position + " , max is :" + (data.size() - 1));
    }

    @Override
    public long getItemId(int position) throws ArrayIndexOutOfBoundsException {
        return getItem(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Holder  holder = null ;
        if(convertView == null)
        {
            View mainview = ConstraintLayout.inflate(mContext,R.layout.list_item_with_image,null);
            holder = new Holder();
            holder.image = mainview.findViewById(R.id.imageView);
            holder.Name = mainview.findViewById(R.id.textViewName);
            holder.Phone = mainview.findViewById(R.id.textViewPhone);

            convertView.setTag(holder);
            // TODO

        }else {
            holder = (Holder) convertView.getTag();
        }

        holder.image.setImageResource(getItem(position).getImageId());
        holder.Name.setText(getItem(position).getName());
        holder.Phone.setText(getItem(position).getPhone());

        return convertView;
    }


    static class Holder {
        private  ImageView image ;
        TextView Name ;
        TextView Phone ;
    }
}
