package com.sdkjordan.alkaff.androidyay.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.net.wifi.WifiManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService2 extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String START_DOWNLOADING = "com.sdkjordan.alkaff.androidyay.services.action.START";
    private static final String CANCEL_DOWNLOADING = "com.sdkjordan.alkaff.androidyay.services.action.CANCEL";

    // TODO: Rename parameters
    private static final String EXTRA_URL = "com.sdkjordan.alkaff.androidyay.services.extra.URL";

    public MyIntentService2() {
        super("MyIntentService2");
    }

    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionDownload(Context context, String url) {
        Intent intent = new Intent(context, MyIntentService2.class);
        intent.setAction(START_DOWNLOADING);
        intent.putExtra(EXTRA_URL, url);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionCancel(Context context) {
        Intent intent = new Intent(context, MyIntentService2.class);
        intent.setAction(CANCEL_DOWNLOADING);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (START_DOWNLOADING.equals(action)) {
                final String url = intent.getStringExtra(EXTRA_URL);
                handleActionStart(url);
            } else if (CANCEL_DOWNLOADING.equals(action)) {
                handleActionCancel();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleActionStart(String url) {
        // TODO: Handle action Foo
        startForeground(1,null);
        throw new UnsupportedOperationException("Not yet implemented");
    }

    /**
     * Handle action Baz in the provided background thread with the provided
     * parameters.
     */
    private void handleActionCancel() {
        // TODO: Handle action Baz
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
