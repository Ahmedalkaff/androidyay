package com.sdkjordan.alkaff.androidyay.eventBusDemo;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;

import com.sdkjordan.alkaff.androidyay.R;

import org.greenrobot.eventbus.EventBus;

public class EventBusActivityB extends AppCompatActivity {

    private EditText editText ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_bus_b);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        editText = findViewById(R.id.editText2);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = editText.getText().toString();
                if(text.length()< 10) {
                    EventBus.getDefault().post(new MessageEvent(editText.getText().toString()));
                }else {
                    EventBus.getDefault().post(new OtherEvent());
                }
                finish();
            }
        });
    }

}
