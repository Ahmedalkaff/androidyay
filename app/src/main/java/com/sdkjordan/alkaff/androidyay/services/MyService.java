package com.sdkjordan.alkaff.androidyay.services;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;

public class MyService extends Service {

    private static final String TAG = MyService.class.getName();


    private static final int START_DOWNLOADING = 1;
    private static final int CLOSE_SEWRVICE =  2;
    MyHandler mHandler;

    @Override
    public void onCreate() {

        HandlerThread thread = new HandlerThread(this.getClass().getName());
        thread.start();
        mHandler = new MyHandler(thread.getLooper());
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String url = intent.getStringExtra("url");
        if (url != null) {
            Message msg = mHandler.obtainMessage(START_DOWNLOADING, url);
            mHandler.sendMessage(msg);
        }


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        mHandler.sendEmptyMessage(CLOSE_SEWRVICE);
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return null;
    }

    class MyHandler extends Handler {

        public MyHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case START_DOWNLOADING:
                    Log.d(TAG, "start downloading :" + msg.obj.toString());
                    break;
                case CLOSE_SEWRVICE:
                    getLooper().quitSafely();

            }
        }
    }
}
