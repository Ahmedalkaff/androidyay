package com.sdkjordan.alkaff.androidyay;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.sdkjordan.alkaff.androidyay.helpers.Constants;

public class MyReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        if(intent != null && intent.getExtras() != null)
        {
            Log.d(Constants.TAG,"Received an intent with data :"+intent.getStringExtra(Constants.Receiver.DATA_KEY));
        }



    }
}
