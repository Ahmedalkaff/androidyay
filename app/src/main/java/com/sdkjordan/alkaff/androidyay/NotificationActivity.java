package com.sdkjordan.alkaff.androidyay;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends AppCompatActivity
        implements DialogInterface.OnClickListener, DialogInterface.OnMultiChoiceClickListener , View.OnClickListener {


    private static final int PENDING_REQ = 10;
    private static final String EXTRA_DATA = "data";
    private static int NOTIFICATION_ID = 100;
    private static final String NOTIFICATION_CHANNEL_1_NAME = "channel1";
    private static final String CHANNEL_1_GROUP = "group1";
    private static final String CHANNEL_1_ID = "channel_one";
    private Notification mNotification;

    private Intent mIntent;
    private PendingIntent mPendingIntent;

    private EditText mNotificationEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = mNotificationEditText.getText().toString();
                if (text != null && text.length() > 0) {

                    showSnackBar(view,text, v-> showToast("Snackbar Action"));
//                    showNotification(text);
//                    mNotificationEditText.setText("");
                }
                //showNameDialogList("Countries", getResources().getStringArray(R.array.countries), true);
            }
        });
        mNotificationEditText = findViewById(R.id.editTextNotificationText);

        String data = getIntent().getStringExtra(EXTRA_DATA);
        if(data != null)
            mNotificationEditText.setText(data);

        createNotificationChannel(
                NOTIFICATION_CHANNEL_1_NAME,
                CHANNEL_1_ID,
                getResources().getString(R.string.channel_description),
                NotificationManager.IMPORTANCE_HIGH);


    }

    private void showSnackBar(View view, String text, View.OnClickListener action) {
        Snackbar.make(view, text, Snackbar.LENGTH_LONG)
                .setAction("Action",action)
                .show();
    }

    @Override
    public void onClick(View v) {

    }

    private void showToast(String text) {
        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP, 0, 0);
        View mToastView = LinearLayout.inflate(getApplicationContext(), R.layout.toast_layout, null);
        TextView textView = mToastView.findViewById(R.id.textViewToast);
        textView.setText(text);
        toast.setView(mToastView);
        toast.show();

    }

    EditText editText;
    AlertDialog.Builder builder;
    AlertDialog alertDialog;

    private void showNameDialog() {
        if (builder == null) {
            builder = new AlertDialog.Builder(this);
            editText = new EditText(this);
            editText.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20F);
            builder.setTitle("Name")
                    .setView(editText)
                    .setPositiveButton("OK", NotificationActivity.this)
                    .setNegativeButton("Cancel", NotificationActivity.this);

        }
        if (alertDialog == null)
            alertDialog = builder.create();

        alertDialog.show();

    }

    AlertDialog.Builder mListBuilder;
    AlertDialog mAlertDialogList;
    boolean mAlterType;

    private void showNameDialogList(String listName, CharSequence[] list, boolean multChoise) {
        mAlterType = multChoise;
        if (builder == null) {
            mListBuilder = new AlertDialog.Builder(this);
            mListBuilder.setTitle(listName);
            if (multChoise)
                mListBuilder.setMultiChoiceItems(list, null, NotificationActivity.this);
            else
                mListBuilder.setSingleChoiceItems(list, -1, NotificationActivity.this);

            mListBuilder.setPositiveButton("OK", NotificationActivity.this)
                    .setNegativeButton("Cancel", NotificationActivity.this);
        }
        if (mAlertDialogList == null)
            mAlertDialogList = mListBuilder.create();

        mAlertDialogList.show();

    }

    int selectIndex = -1;

    @Override
    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case DialogInterface.BUTTON_POSITIVE:
                if (dialog == alertDialog) {
                    if (editText != null)
                        showToast(editText.getText().toString());
                } else if (dialog == mAlertDialogList) {
                    if (mAlterType) {
                        if (mCheckItems.size() > 0) {
                            String[] list = getResources().getStringArray(R.array.countries);
                            StringBuffer stringBuffer = new StringBuffer();
                            for (Integer i : mCheckItems) {
                                stringBuffer.append(list[i] + ", ");
                            }
                            int index = stringBuffer.lastIndexOf(", ");
                            stringBuffer = new StringBuffer(stringBuffer.substring(0, index));
                            if (mCheckItems.size() > 1) {
                                index = stringBuffer.lastIndexOf(", ");
                                stringBuffer.replace(index, index + 2, " and ");
                            }
                            showToast(stringBuffer.toString());
                        } else {
                            showToast("No select data");
                        }
                    } else {
                        if (selectIndex != -1)
                            showToast(getResources().getStringArray(R.array.countries)[selectIndex]);
                        else
                            showToast("No select data");
                    }

                }
                break;
            case DialogInterface.BUTTON_NEGATIVE:
                dialog.dismiss();
                break;
            case DialogInterface.BUTTON_NEUTRAL:
                break;
            default:
                selectIndex = which;

        }
    }

    List<Integer> mCheckItems = new ArrayList<>();

    @Override
    public void onClick(DialogInterface dialog, int which, boolean isChecked) {

        if (isChecked)
            mCheckItems.add(which);
        else
            mCheckItems.remove(which);
    }

    private void createNotificationChannel(String channelName, String channelID, String channelDescription, int importance) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            NotificationChannel channel = new NotificationChannel(channelID, channelName, importance);
            channel.setDescription(channelDescription);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private Notification.Builder mNotBuilder;
    NotificationManagerCompat notificationManager;

    private void showNotification(String text) {

        if (mNotBuilder == null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mNotBuilder = new Notification.Builder(getApplicationContext(), NOTIFICATION_CHANNEL_1_NAME);
                mNotBuilder.setGroup(CHANNEL_1_GROUP);
                mNotBuilder.setChannelId(CHANNEL_1_ID);
            } else {
                mNotBuilder = new Notification.Builder(getApplicationContext());
            }

            mNotBuilder.setTicker(getResources().getString(R.string.app_name))
                    .setWhen(System.currentTimeMillis())
                    //.setProgress(100, 0, true)
                    .setContentTitle(getResources().getString(R.string.app_name))
                    .setSmallIcon(R.mipmap.ic_launcher);


        }

        mIntent = new Intent(NotificationActivity.this, NotificationActivity.class);
        mIntent.putExtra(EXTRA_DATA, text);
        mPendingIntent = PendingIntent.getActivity(getApplicationContext(), PENDING_REQ, mIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        mNotification =
                mNotBuilder.setContentIntent(mPendingIntent)
                        .setContentText(text)
                        .build();


        if (notificationManager == null)
            notificationManager = NotificationManagerCompat.from(this);

        notificationManager.notify(NOTIFICATION_ID++, mNotification);
    }


}
