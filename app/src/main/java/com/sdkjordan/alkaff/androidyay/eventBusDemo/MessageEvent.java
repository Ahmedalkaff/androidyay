package com.sdkjordan.alkaff.androidyay.eventBusDemo;

public class MessageEvent {

    private  String msg ;

    public MessageEvent(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}
