package com.sdkjordan.alkaff.androidyay.helpers;

import android.provider.ContactsContract;

public class Record {

    private static int _id = 0 ;
    private int id ;
    private String Name ;
    private String Phone ;
    private int ImageId ;

    public Record(String name, String phone, int imageId) {
        id = _id++ ;
       setName(name);
       setPhone(phone);
       setImageId(imageId);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public int getImageId() {
        return ImageId;
    }

    public void setImageId(int imageId) {
        ImageId = imageId;
    }
}
