package com.sdkjordan.alkaff.androidyay.threading;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.sdkjordan.alkaff.androidyay.R;

public class ThreadingActivity extends AppCompatActivity implements  AdapterView.OnItemSelectedListener ,Handler.Callback {

    private static final int NO_THREADING = 0 ;
    private static final int WITHTHREADING_WRONG = 1 ;
    private static final int RUN_ON_UI_THREAD = 2 ;
    private static final int VIEW_POST = 3 ;
    private static final int ASYNC_TASK = 4 ;
    private static final int NO_ACTION = 5 ;


    // TODO: this is a new change 
    private static  final  int WHAT_ACTION_1 = 1 ;
    private static  final  int WHAT_ACTION_2 = 2 ;
    private static  final  int WHAT_ACTION_3 = 3 ;

    private static final int HANDLER = 5 ;

    private static final long DELAY = 10000 ;
    private static final String TAG = "ThreadingActivity" ;

    private int action = NO_THREADING ;

    ProgressBar mProgressBar ;
    private Spinner mSpinner;
    private ImageView mImageView ;

    private Handler mHandler ;

    private  ImageLoaderAsyncTask mImageLoaderAsyncTask ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_threading);

        mSpinner = findViewById(R.id.spinner);
        mSpinner.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, getResources().getStringArray(R.array.spinner_items)));
        mSpinner.setOnItemSelectedListener(this);
        mImageView = findViewById(R.id.imageView);
        mProgressBar = findViewById(R.id.progressBar);

        mImageLoaderAsyncTask = new ImageLoaderAsyncTask();


    }

    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.buttonLoad:
                performAction(action);
                break;
            case R.id.buttonToast:
                showToast("Im working..");
        }
    }

    private void performAction(int action) {
        switch (action)
        {
            case NO_THREADING :
                NoThreadingLoad();
                break;
            case WITHTHREADING_WRONG:
                WithThreadingLoad();
                break;
            case RUN_ON_UI_THREAD :
                usingRunOnUI();
                break;
            case VIEW_POST :
                usingViewPost();
                break;
            case ASYNC_TASK :
                usingAsyncTask();
                break;
            case HANDLER :
                usingHandler();
                break;
                default:
                    showToast("No action was selected");
        }
    }

    private void usingHandler() {

        HandlerThread thread = new HandlerThread("HandlerThread",Thread.NORM_PRIORITY);
        thread.start();
        Looper looper = thread.getLooper() ;
        MyHandler  handler = new MyHandler(looper,this) ;


        // from Main UI thread you can use
       Message message =  mHandler.obtainMessage(WHAT_ACTION_1);
        handler.sendMessage(message);



    }

    @Override
    public boolean handleMessage(Message msg) {
        return false;
    }

    class MyHandler extends Handler {

        ThreadingActivity mThreadingActivity ;
        public MyHandler(Looper looper) {
            super(looper);
        }

        public MyHandler(Looper looper, ThreadingActivity threadingActivity) {
            super(looper,threadingActivity);
            mThreadingActivity = threadingActivity ;



        }

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what)
            {
                case WHAT_ACTION_1:
                    doFirstAction();
                    break;
                case  WHAT_ACTION_2:
                    doSecondAction();
                    break;
                case  WHAT_ACTION_3:
                    doThirdAction();
                    break;
            }
        }

        private void doThirdAction() {
            mThreadingActivity.loadImage();
        }

        private void doSecondAction() {

        }

        private void doFirstAction() {

        }
    }

    private void usingAsyncTask() {

        if(mImageLoaderAsyncTask.getStatus().equals(AsyncTask.Status.FINISHED))
        {
            mImageLoaderAsyncTask.execute(R.raw.tiger);
        }else {
            showToast("Image i loading, please wait");
        }

    }

    private void usingViewPost() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                sleep(DELAY);

                // this will run on the Main UiThread
                mImageView.post(new Runnable() {
                    @Override
                    public void run() {
                        loadImage();
                    }
                }) ;
            }
        });
        thread.start();
    }

    private void usingRunOnUI() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                sleep(DELAY);

                // this will run on the Main UiThread
               runOnUiThread(new Runnable() {
                   @Override
                   public void run() {
                       loadImage();
                   }
               });
            }
        });
        thread.start();
    }

    private void WithThreadingLoad() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                NoThreadingLoad();
            }
        });
        thread.start();

    }

    private void NoThreadingLoad() {
        sleep(DELAY);
        loadImage();
    }


    private void sleep(long delay)
    {
        mProgressBar.post(new Runnable() {
            @Override
            public void run() {
                mProgressBar.setProgress(0);
            }
        });
        long sleepPeriod = delay /100 ;
        try {
            for(int i=1;i<=100;i++) {
                Thread.sleep(sleepPeriod);
                final int progress = i ;
                mProgressBar.post(new Runnable() {
                    @Override
                    public void run() {
                        mProgressBar.setProgress(progress);
                    }
                }) ;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    private void loadImage()
    {
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.raw.tiger) ;
        mImageView.setImageBitmap(bitmap);
    }

    private void showToast(String text)
    {
        Toast toast = Toast.makeText(getApplicationContext(),text,Toast.LENGTH_SHORT);

        toast.show();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        action = position ;
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        action = NO_ACTION ;
    }


    class ImageLoaderAsyncTask extends AsyncTask<Integer,Float,Bitmap>
    {
        @Override
        protected void onPreExecute() {         // MainUI
            mProgressBar.setProgress(0);
            mProgressBar.setMax(100);
        }

        @Override
        protected Bitmap doInBackground(Integer... integers) {      // Run int the worker thread


            Log.w(TAG,String.format("%-20s : %s",Thread.currentThread().getName(),"doInBackground")) ;
            try {
                long sleepPeriod = DELAY /100 ;
                for(int i=1;i<=100;i++) {
                    Thread.sleep(sleepPeriod);
                    publishProgress((float) (i/100.0F));           // will call onProgressUpdate
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return BitmapFactory.decodeResource(getResources(),integers[0]);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {           // MainUI
            mImageView.setImageBitmap(bitmap);
        }

        @Override
        protected void onProgressUpdate(Float... values) {       // MainUI
            mProgressBar.setProgress((int)(values[0]*100));
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            mImageView.setImageBitmap(bitmap);
        }

        @Override
        protected void onCancelled() {
            mImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(),R.raw.android));
        }


    }
}
