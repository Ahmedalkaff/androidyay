package com.sdkjordan.alkaff.androidyay;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class MyListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {


    private static final String TAG = "MyListActivity";

    public static final String KEY_NAME = "name";

    public static final String KEY_PHONE = "phone";

    private static final String[] from =  {KEY_NAME , KEY_PHONE};
    private static final  int[] to = {R.id.editText, R.id.textView3};



    ListView mlistView;

    private  ArrayList<String> data = new ArrayList<>();
    private ArrayAdapter<String> mArrayAdapter ;


    private ArrayList<TreeMap<String,String>> dataMaps = new ArrayList<>();
    private SimpleAdapter simpleAdapter ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        data.add("A");


        mArrayAdapter = new ArrayAdapter<String>(MyListActivity.this, android.R.layout.simple_list_item_1, getResources().getStringArray(R.array.countries)) ;
        //mArrayAdapter = new ArrayAdapter<String>(MyListActivity.this, R.layout.item_list,R.id.editText, data) ;


        //simpleAdapter = new SimpleAdapter(getApplicationContext(),dataMaps,R.layout.item_list,from,to);
        //mlistView.setAdapter(simpleAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                char last = data.get(data.size() - 1).charAt(0);
                int ascii = (int) last;
                ascii++;
                char newchar = (char) ascii;
                data.add(String.valueOf(newchar));


                mArrayAdapter.notifyDataSetChanged();
                Log.d(TAG, data.toString());
            }
        });


        init();
    }

    private void init() {

        mlistView = findViewById(R.id.listview);
        mlistView.setAdapter(mArrayAdapter);

        mlistView.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(getApplicationContext(), data.get(position), Toast.LENGTH_SHORT).show();

//        TextView textView = view.findViewById(android.R.id.text1);
//        String tex = textView.getText().toString();
//
//        Toast.makeText(getApplicationContext(),tex,Toast.LENGTH_SHORT).show();
    }
}
