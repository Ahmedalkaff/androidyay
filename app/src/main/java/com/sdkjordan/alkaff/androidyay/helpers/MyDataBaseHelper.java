package com.sdkjordan.alkaff.androidyay.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDataBaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "myDatabase_db";
    public static final String TABLE_NAME = "[Users]";
    public static final String USER_ID = "[user_id]";
    public static final String USER_NAME = "[User name]";
    public static final String USER_PASSWORD = "[Password]";
    public static final String USER_EMAIL = "[Email]";
    public static final String USER_PHONE = "[Phone]";


    private static int Version = 1;

    private SQLiteDatabase database;

    public MyDataBaseHelper(Context context) {
        super(context, DATABASE_NAME, null, Version);
    }

    private static final String CREATE_TABLE_USERS = String.format(
            "CREATE TABLE IF NOT EXISTS %s (" +
                    "%s integer PRIMARY KEY AUTOINCREMENT," +
                    "%s text NOT NULL," +
                    "%s text NOT NULL," +
                    "%s text NOT NULL UNIQUE," +
                    "%s text NOT NULL UNIQUE ) ;",
            TABLE_NAME, USER_ID, USER_NAME, USER_PASSWORD, USER_EMAIL, USER_PHONE);


    public long AddUser(String name, String password, String email, String phone) {

        if (database == null || !database.isOpen() || database.isReadOnly())
            database = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(USER_NAME, name);
        values.put(USER_PASSWORD, password);
        values.put(USER_EMAIL, email);
        values.put(USER_PHONE, phone);
        return database.insert(MyDataBaseHelper.TABLE_NAME, null, values);
    }

    public Cursor GetUserInfo(String name ,String pass)
    {
        String[] selectionArgs  = {name,pass} ;
        String query = "SELECT * FROM "+ TABLE_NAME + " where "+USER_NAME+" = ? AND "+USER_PASSWORD+" =? ;" ;
        Cursor c =  database.rawQuery(query,selectionArgs);
        // OR
        String selection = USER_NAME+"= ? AND "+USER_PASSWORD+"=?";
        c = database.query(TABLE_NAME,null,selection,selectionArgs,null,null,null);
        return  c ;
    }
    public Cursor getAllUsers()
    {
        if(database == null || !database.isOpen() )
            database = getReadableDatabase();

        String[] columns = {USER_ID,USER_NAME,USER_EMAIL,USER_PHONE};
        return database.query(TABLE_NAME,columns,null,null,null,null,null);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        database = db;
        db.execSQL(CREATE_TABLE_USERS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            // TODO : 1) Create the new database
            // TODO : 2) Move the old date from the old database to the new one
            // TODO : 3) Delete the old database

            // Other option is to delete the old database and create a new one
        }
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }
}
