package com.sdkjordan.alkaff.androidyay;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.drm.DrmStore;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.DexterBuilder;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

public class MainActivity extends AppCompatActivity
        implements View.OnClickListener,
        View.OnLongClickListener {

    private static final String TAG = "androidyay";
    private static final String MY_ACTION = "com.sdkjordan.alkaff.androidyay.do_action";
    private static final int REQUEST_CODE = 10;
    private static final int ANOTHER_REQUEST_CODE = 15 ;
    private static final int EQYEST_PER_CALL =  100;

    TextView mTextView ;
    Button mButtonAdd, mButton ;
    PermissionListener  listener ;

    View.OnClickListener onClickListener ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Log.d(TAG,this.getClass().getName()+": onCreate(Bundle savedInstanceState)");

//        listener = new PermissionListener() {
//
//            @Override
//            public void onPermissionGranted(PermissionGrantedResponse response) {
//                MakeACall("0788686870");
//
//            }
//
//            @Override
//            public void onPermissionDenied(PermissionDeniedResponse response) {
//
//            }
//
//            @Override
//            public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
//
//            }
//        };
        inti() ;
//        onClickListener = new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switch (v.getId())
//                {
//                    case  R.id.button15:
//                        Toast.makeText(getApplicationContext(),"Button 15 is clicked",Toast.LENGTH_SHORT).show();
//
//                        Intent intent = new Intent(MainActivity.this,MyActivity.class);
//                        // Intent intent = new Intent(MY_ACTION);
//                        intent.putExtra("data","this is the data");
//                        intent.putExtra("number",10);
//                        startActivity(intent);
//                        break;
//                    case R.id.buttonAdd:
//                        Toast.makeText(getApplicationContext(),"Button 16 is clicked",Toast.LENGTH_SHORT).show();
//                        Intent intent1 = new Intent(MY_ACTION);
//                        intent1.putExtra("data","this is the data");
//                        //intent1.setData(Uri.parse("http://www.sdkjordan.com"));
//                        startActivityForResult(intent1,REQUEST_CODE);
//
//                        break;
//                }
//            }
//        };

        mButtonAdd.setOnClickListener(this);
        mButtonAdd.setOnLongClickListener(this);
        mButton.setOnClickListener(this);
    }


    private void inti()
    {
        mTextView = findViewById(R.id.textView);
        mButtonAdd = findViewById(R.id.buttonAdd);
        mButton = findViewById(R.id.button15);
    }
    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,this.getClass().getName()+": onStart()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,this.getClass().getName()+": onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,this.getClass().getName()+": onDestroy()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,this.getClass().getName()+": onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,this.getClass().getName()+": onResume()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,this.getClass().getName()+": onRestart");
    }

//    public void doAction(View v)
//    {
//        if(v instanceof Button)
//            ((Button)v).setText("Clicked");
//        switch (v.getId())
//        {
//            case  R.id.button15:
//                Toast.makeText(getApplicationContext(),"Button 15 is clicked",Toast.LENGTH_SHORT).show();
//
//                Intent intent = new Intent(MainActivity.this,MyActivity.class);
//               // Intent intent = new Intent(MY_ACTION);
//                intent.putExtra("data","this is the data");
//                intent.putExtra("number",10);
//                startActivity(intent);
//                break;
//            case R.id.buttonAdd:
//                Toast.makeText(getApplicationContext(),"Button 16 is clicked",Toast.LENGTH_SHORT).show();
//                Intent intent1 = new Intent(MY_ACTION);
//                intent1.putExtra("data","this is the data");
//                //intent1.setData(Uri.parse("http://www.sdkjordan.com"));
//                startActivityForResult(intent1,REQUEST_CODE);
//
//                break;
//        }
//
//    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      switch (requestCode)
      {
          case REQUEST_CODE:
              if(resultCode == RESULT_CANCELED)
              {
                  mTextView.setText("The process failed!");
              }else if(resultCode == RESULT_OK)
              {
                  String result = data.getStringExtra("result");
                  mTextView.setText("The result:"+result);
              }
              break;
          case ANOTHER_REQUEST_CODE:

      }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case  R.id.button15:
                Toast.makeText(getApplicationContext(),"Button 15 is clicked",Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(MainActivity.this,MyActivity.class);
                // Intent intent = new Intent(MY_ACTION);
                intent.putExtra("data","this is the data");
                intent.putExtra("number",10);
                startActivity(intent);
                break;
            case R.id.buttonAdd:
                Toast.makeText(getApplicationContext(),"Button 16 is clicked",Toast.LENGTH_SHORT).show();
                //Intent intent1 = new Intent(Intent.ACTION_CALL);
                //intent1.putExtra("data","this is the data");



//                Dexter.withActivity(MainActivity.this)
//                        .withPermission(Manifest.permission.CALL_PHONE)
//                        .withListener(listener)
//                        .check();
//                MakeACall("0788686870");

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M ) {
                    // API 23 or above
                    if (checkSelfPermission(Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED ||
                            checkSelfPermission(Manifest.permission.WRITE_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                        // Permission is not granted
                        if(shouldShowRequestPermissionRationale(Manifest.permission.CALL_PHONE) ||
                                shouldShowRequestPermissionRationale(Manifest.permission.WRITE_CONTACTS) )
                        {
                            // TODO : show why that permission is needed
                            Toast.makeText(getApplicationContext(),"We need this permission to  make the call",Toast.LENGTH_SHORT).show();
                        }
                        requestPermissions(new String[] {Manifest.permission.CALL_PHONE, Manifest.permission.WRITE_CONTACTS},EQYEST_PER_CALL);
                        return;

                    } else {
                        // Permission is  granted
                        MakeACall("0788686870");
                    }
                }else {
                    // API less than 23
                    MakeACall("0788686870");
                }

                //startActivityForResult(intent1,REQUEST_CODE);

                break;
        }
    }

    @SuppressLint("MissingPermission")
    private void MakeACall(String number)
    {
        Intent intent1 = new Intent(Intent.ACTION_CALL);
        intent1.setData(Uri.parse("tel:"+number));
        startActivity(intent1);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode)
        {
            case EQYEST_PER_CALL:
                if(grantResults != null && grantResults.length > 0)
                {
                    for(int result : grantResults)
                    {
                        if(result  == PackageManager.PERMISSION_GRANTED)
                        {
                            // TODO : stop the action
                            return;
                        }
                    }
                        MakeACall("0788686870");
                }
                break;
        }
    }

    @Override
    public boolean onLongClick(View v) {

        return false;
    }
}
