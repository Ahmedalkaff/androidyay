package com.sdkjordan.alkaff.androidyay;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MyActivity extends AppCompatActivity {
    private static final String TAG = "androidyay";

    String data ;
    Button mButton ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        Log.d(TAG,this.getClass().getName()+": onCreate(Bundle savedInstanceState)");

        mButton = findViewById(R.id.buttonDoAction);
        Intent intent = getIntent();
        int number = intent.getIntExtra("number",0);
        data = intent.getStringExtra("data") ;

       // data = intent.getDataString();


        if(data != null)
            mButton.setText(data+ ":"+number );

    }

    public void OnClick(View v)
    {
        switch (v.getId())
        {
            case  R.id.buttonDoAction:
                String result = null ;
                if(data != null)
                {
                    result =  String.valueOf(data.hashCode()) ;
                }
                Intent intent = new Intent();
                intent.putExtra("result",result);
                setResult(RESULT_OK,intent);
                break;
            case R.id.buttonCancel:
                setResult(RESULT_CANCELED);

                break;
        }
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,this.getClass().getName()+": onStart()");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,this.getClass().getName()+": onStop()");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,this.getClass().getName()+": onDestroy()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,this.getClass().getName()+": onPause()");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG,this.getClass().getName()+": onResume()");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TAG,this.getClass().getName()+": onRestart");
    }
}
