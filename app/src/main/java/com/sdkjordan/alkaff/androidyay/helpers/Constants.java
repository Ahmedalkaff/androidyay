package com.sdkjordan.alkaff.androidyay.helpers;

public class Constants {

    public static final String TAG = "androidyay" ;

    public interface Receiver {
       String DATA_KEY = "data";
        String ACTION = "com.sdkjordan.alkaff.androidyay.myAction";
    }
}
